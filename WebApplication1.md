<a name='assembly'></a>
# WebApplication1

## Contents

- [Math](#T-WebApplication1-Math 'WebApplication1.Math')
  - [Add(a,b)](#M-WebApplication1-Math-Add-System-Int32,System-Int32- 'WebApplication1.Math.Add(System.Int32,System.Int32)')
  - [Add(a,b)](#M-WebApplication1-Math-Add-System-Double,System-Double- 'WebApplication1.Math.Add(System.Double,System.Double)')
  - [Divide(a,b)](#M-WebApplication1-Math-Divide-System-Int32,System-Int32- 'WebApplication1.Math.Divide(System.Int32,System.Int32)')
  - [Divide(a,b)](#M-WebApplication1-Math-Divide-System-Double,System-Double- 'WebApplication1.Math.Divide(System.Double,System.Double)')
  - [Multiply(a,b)](#M-WebApplication1-Math-Multiply-System-Int32,System-Int32- 'WebApplication1.Math.Multiply(System.Int32,System.Int32)')
  - [Multiply(a,b)](#M-WebApplication1-Math-Multiply-System-Double,System-Double- 'WebApplication1.Math.Multiply(System.Double,System.Double)')
  - [Subtract(a,b)](#M-WebApplication1-Math-Subtract-System-Int32,System-Int32- 'WebApplication1.Math.Subtract(System.Int32,System.Int32)')
  - [Subtract(a,b)](#M-WebApplication1-Math-Subtract-System-Double,System-Double- 'WebApplication1.Math.Subtract(System.Double,System.Double)')
- [ValuesController](#T-WebApplication1-Controllers-ValuesController 'WebApplication1.Controllers.ValuesController')
  - [Delete(id)](#M-WebApplication1-Controllers-ValuesController-Delete-System-Int32- 'WebApplication1.Controllers.ValuesController.Delete(System.Int32)')
  - [Get()](#M-WebApplication1-Controllers-ValuesController-Get 'WebApplication1.Controllers.ValuesController.Get')
  - [Get(id)](#M-WebApplication1-Controllers-ValuesController-Get-System-Int32- 'WebApplication1.Controllers.ValuesController.Get(System.Int32)')
  - [Post(value)](#M-WebApplication1-Controllers-ValuesController-Post-System-String- 'WebApplication1.Controllers.ValuesController.Post(System.String)')
  - [Put(id,value)](#M-WebApplication1-Controllers-ValuesController-Put-System-Int32,System-String- 'WebApplication1.Controllers.ValuesController.Put(System.Int32,System.String)')

<a name='T-WebApplication1-Math'></a>
## Math `type`

##### Namespace

WebApplication1

<a name='M-WebApplication1-Math-Add-System-Int32,System-Int32-'></a>
### Add(a,b) `method`

##### Summary

Adds two integers `a`and `b`and returns the result.

##### Returns

The sum of two integers.

##### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| a | [System.Int32](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Int32 'System.Int32') | An integer. |
| b | [System.Int32](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Int32 'System.Int32') | An integer. |

##### Exceptions

| Name | Description |
| ---- | ----------- |
| [System.OverflowException](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.OverflowException 'System.OverflowException') | Thrown when one parameter is max 
and the other is greater than 0. |

##### Example

```
int c = Math.Add(4, 5);
if (c &gt; 10)
{
    Console.WriteLine(c);
} 
```

##### See Also

- [WebApplication1.Math.Subtract](#M-WebApplication1-Math-Subtract-System-Int32,System-Int32- 'WebApplication1.Math.Subtract(System.Int32,System.Int32)')
- [WebApplication1.Math.Multiply](#M-WebApplication1-Math-Multiply-System-Int32,System-Int32- 'WebApplication1.Math.Multiply(System.Int32,System.Int32)')
- [WebApplication1.Math.Divide](#M-WebApplication1-Math-Divide-System-Int32,System-Int32- 'WebApplication1.Math.Divide(System.Int32,System.Int32)')

<a name='M-WebApplication1-Math-Add-System-Double,System-Double-'></a>
### Add(a,b) `method`

##### Summary

Adds two doubles `a`and `b`and returns the result.

##### Returns

The sum of two doubles.

##### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| a | [System.Double](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Double 'System.Double') | A double precision number. |
| b | [System.Double](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Double 'System.Double') | A double precision number. |

##### Exceptions

| Name | Description |
| ---- | ----------- |
| [System.OverflowException](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.OverflowException 'System.OverflowException') | Thrown when one parameter is max 
and the other is greater than 0. |

##### Example

```
double c = Math.Add(4.5, 5.4);
if (c &gt; 10)
{
    Console.WriteLine(c);
} 
```

##### See Also

- [WebApplication1.Math.Subtract](#M-WebApplication1-Math-Subtract-System-Double,System-Double- 'WebApplication1.Math.Subtract(System.Double,System.Double)')
- [WebApplication1.Math.Multiply](#M-WebApplication1-Math-Multiply-System-Double,System-Double- 'WebApplication1.Math.Multiply(System.Double,System.Double)')
- [WebApplication1.Math.Divide](#M-WebApplication1-Math-Divide-System-Double,System-Double- 'WebApplication1.Math.Divide(System.Double,System.Double)')

<a name='M-WebApplication1-Math-Divide-System-Int32,System-Int32-'></a>
### Divide(a,b) `method`

##### Summary

Divides an integer `a`by another integer `b`and returns the result.

##### Returns

The quotient of two integers.

##### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| a | [System.Int32](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Int32 'System.Int32') | An integer dividend. |
| b | [System.Int32](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Int32 'System.Int32') | An integer divisor. |

##### Exceptions

| Name | Description |
| ---- | ----------- |
| [System.DivideByZeroException](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.DivideByZeroException 'System.DivideByZeroException') | Thrown when `b`is equal to 0. |

##### Example

```
int c = Math.Divide(4, 5);
if (c &gt; 1)
{
    Console.WriteLine(c);
} 
```

##### See Also

- [WebApplication1.Math.Add](#M-WebApplication1-Math-Add-System-Int32,System-Int32- 'WebApplication1.Math.Add(System.Int32,System.Int32)')
- [WebApplication1.Math.Subtract](#M-WebApplication1-Math-Subtract-System-Int32,System-Int32- 'WebApplication1.Math.Subtract(System.Int32,System.Int32)')
- [WebApplication1.Math.Multiply](#M-WebApplication1-Math-Multiply-System-Int32,System-Int32- 'WebApplication1.Math.Multiply(System.Int32,System.Int32)')

<a name='M-WebApplication1-Math-Divide-System-Double,System-Double-'></a>
### Divide(a,b) `method`

##### Summary

Divides a double `a`by another double `b`and returns the result.

##### Returns

The quotient of two doubles.

##### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| a | [System.Double](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Double 'System.Double') | A double precision dividend. |
| b | [System.Double](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Double 'System.Double') | A double precision divisor. |

##### Exceptions

| Name | Description |
| ---- | ----------- |
| [System.DivideByZeroException](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.DivideByZeroException 'System.DivideByZeroException') | Thrown when `b`is equal to 0. |

##### Example

```
double c = Math.Divide(4.5, 5.4);
if (c &gt; 1.0)
{
    Console.WriteLine(c);
} 
```

##### See Also

- [WebApplication1.Math.Add](#M-WebApplication1-Math-Add-System-Double,System-Double- 'WebApplication1.Math.Add(System.Double,System.Double)')
- [WebApplication1.Math.Subtract](#M-WebApplication1-Math-Subtract-System-Double,System-Double- 'WebApplication1.Math.Subtract(System.Double,System.Double)')
- [WebApplication1.Math.Multiply](#M-WebApplication1-Math-Multiply-System-Double,System-Double- 'WebApplication1.Math.Multiply(System.Double,System.Double)')

<a name='M-WebApplication1-Math-Multiply-System-Int32,System-Int32-'></a>
### Multiply(a,b) `method`

##### Summary

Multiplies two integers `a`and `b`and returns the result.

##### Returns

The product of two integers.

##### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| a | [System.Int32](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Int32 'System.Int32') | An integer. |
| b | [System.Int32](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Int32 'System.Int32') | An integer. |

##### Example

```
int c = Math.Multiply(4, 5);
if (c &gt; 100)
{
    Console.WriteLine(c);
} 
```

##### See Also

- [WebApplication1.Math.Add](#M-WebApplication1-Math-Add-System-Int32,System-Int32- 'WebApplication1.Math.Add(System.Int32,System.Int32)')
- [WebApplication1.Math.Subtract](#M-WebApplication1-Math-Subtract-System-Int32,System-Int32- 'WebApplication1.Math.Subtract(System.Int32,System.Int32)')
- [WebApplication1.Math.Divide](#M-WebApplication1-Math-Divide-System-Int32,System-Int32- 'WebApplication1.Math.Divide(System.Int32,System.Int32)')

<a name='M-WebApplication1-Math-Multiply-System-Double,System-Double-'></a>
### Multiply(a,b) `method`

##### Summary

Multiplies two doubles `a`and `b`and returns the result.

##### Returns

The product of two doubles.

##### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| a | [System.Double](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Double 'System.Double') | A double precision number. |
| b | [System.Double](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Double 'System.Double') | A double precision number. |

##### Example

```
double c = Math.Multiply(4.5, 5.4);
if (c &gt; 100.0)
{
    Console.WriteLine(c);
} 
```

##### See Also

- [WebApplication1.Math.Add](#M-WebApplication1-Math-Add-System-Double,System-Double- 'WebApplication1.Math.Add(System.Double,System.Double)')
- [WebApplication1.Math.Subtract](#M-WebApplication1-Math-Subtract-System-Double,System-Double- 'WebApplication1.Math.Subtract(System.Double,System.Double)')
- [WebApplication1.Math.Divide](#M-WebApplication1-Math-Divide-System-Double,System-Double- 'WebApplication1.Math.Divide(System.Double,System.Double)')

<a name='M-WebApplication1-Math-Subtract-System-Int32,System-Int32-'></a>
### Subtract(a,b) `method`

##### Summary

Subtracts `b`from `a`and returns the result.

##### Returns

The difference between two integers.

##### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| a | [System.Int32](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Int32 'System.Int32') | An integer. |
| b | [System.Int32](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Int32 'System.Int32') | An integer. |

##### Example

```
int c = Math.Subtract(4, 5);
if (c &gt; 1)
{
    Console.WriteLine(c);
} 
```

##### See Also

- [WebApplication1.Math.Add](#M-WebApplication1-Math-Add-System-Int32,System-Int32- 'WebApplication1.Math.Add(System.Int32,System.Int32)')
- [WebApplication1.Math.Multiply](#M-WebApplication1-Math-Multiply-System-Int32,System-Int32- 'WebApplication1.Math.Multiply(System.Int32,System.Int32)')
- [WebApplication1.Math.Divide](#M-WebApplication1-Math-Divide-System-Int32,System-Int32- 'WebApplication1.Math.Divide(System.Int32,System.Int32)')

<a name='M-WebApplication1-Math-Subtract-System-Double,System-Double-'></a>
### Subtract(a,b) `method`

##### Summary

Subtracts a double `b`from another double `a`and returns the result.

##### Returns

The difference between two doubles.

##### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| a | [System.Double](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Double 'System.Double') | A double precision number. |
| b | [System.Double](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Double 'System.Double') | A double precision number. |

##### Example

```
double c = Math.Subtract(4.5, 5.4);
if (c &gt; 1)
{
    Console.WriteLine(c);
} 
```

##### See Also

- [WebApplication1.Math.Add](#M-WebApplication1-Math-Add-System-Double,System-Double- 'WebApplication1.Math.Add(System.Double,System.Double)')
- [WebApplication1.Math.Multiply](#M-WebApplication1-Math-Multiply-System-Double,System-Double- 'WebApplication1.Math.Multiply(System.Double,System.Double)')
- [WebApplication1.Math.Divide](#M-WebApplication1-Math-Divide-System-Double,System-Double- 'WebApplication1.Math.Divide(System.Double,System.Double)')

<a name='T-WebApplication1-Controllers-ValuesController'></a>
## ValuesController `type`

##### Namespace

WebApplication1.Controllers

<a name='M-WebApplication1-Controllers-ValuesController-Delete-System-Int32-'></a>
### Delete(id) `method`

##### Summary

DELETE api/values/5

##### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| id | [System.Int32](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Int32 'System.Int32') |  |

<a name='M-WebApplication1-Controllers-ValuesController-Get'></a>
### Get() `method`

##### Summary

GET api/values

##### Returns

all content in string

##### Parameters

This method has no parameters.

<a name='M-WebApplication1-Controllers-ValuesController-Get-System-Int32-'></a>
### Get(id) `method`

##### Summary

GET api/values/5

##### Returns

specific content in string

##### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| id | [System.Int32](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Int32 'System.Int32') | identifier in int |

<a name='M-WebApplication1-Controllers-ValuesController-Post-System-String-'></a>
### Post(value) `method`

##### Summary

POST api/values

##### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| value | [System.String](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.String 'System.String') | post in string |

<a name='M-WebApplication1-Controllers-ValuesController-Put-System-Int32,System-String-'></a>
### Put(id,value) `method`

##### Summary

PUT api/values/5

##### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| id | [System.Int32](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.Int32 'System.Int32') | identifier in int |
| value | [System.String](http://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k:System.String 'System.String') | specific content in string |
